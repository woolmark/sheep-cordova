
function Sheep() {
  this.DURATION = 3000,
  this.count = 0,
  this.flagNewSheep;

  var self = this,
      sheep00 = document.getElementsByClassName("sheep00")[0];
  self.registerCounterListener(sheep00);

  [].forEach.call(document.getElementsByClassName("woolmark_canvas"), function(canvas) {
    canvas.addEventListener("touchstart", function(e) {
      self.flagNewSheep = true;
      self.addNewSheep();
    }, false);
    canvas.addEventListener("touchcancel", function(e) {
      self.flagNewSheep = false;
    }, false);
    canvas.addEventListener("touchend", function(e) {
      self.flagNewSheep = false;
    }, false);
    canvas.onmousedown = function(e) {
      self.flagNewSheep = true;
      self.addNewSheep();
    };
    canvas.onmouseup = function(e) {
      self.flagNewSheep = false;
    };
  });
}

Sheep.prototype.addNewSheep = function() {
  var self = this,
      sheep,
      sheepIndex;
  if (self.flagNewSheep) {
    sheep = document.createElement("div");

    sheepIndex = 1 + Math.floor(Math.random() * 98);

    sheep.className = "sheep sheep" + ((sheepIndex < 10) ? "0" + sheepIndex : sheepIndex);
    sheep.innerHTML = "&nbsp;";
    self.registerCounterListener(sheep);
    sheep.addEventListener("animationend", function(e) {
      self.onSheepAnimationEnd(e);
    }, false);
    sheep.addEventListener("webkitAnimationEnd", function(e) {
      self.onSheepAnimationEnd(e);
    }, false);

    document.getElementsByClassName("woolmark_canvas")[0].appendChild(sheep);

    setTimeout(function() {
      self.addNewSheep();
    }, 100);
  }
};

Sheep.prototype.registerCounterListener = function(sheep) {
  var self = this;
  sheep.addEventListener("animationstart", function(e) {
    self.onSheepAnimationStart(e);
  }, false);
  sheep.addEventListener("animationiteration", function(e) {
    self.onSheepAnimationInteration(e);
  });
  sheep.addEventListener("webkitAnimationStart", function(e) {
    self.onSheepAnimationStart(e);
  }, false);
  sheep.addEventListener("webkitAnimationIteration", function(e) {
    self.onSheepAnimationInteration(e);
  });
};

Sheep.prototype.onSheepAnimationStart = function(e) {
  var self = this;
  if (e.animationName.indexOf("sheep-run") === 0) {
    if (e.srcElement) {
      e.srcElement.style.visibility = "visible";
    } else if (e.originalTarget) {
      e.originalTarget.style.visibility = "visible";
    }

    setTimeout(function() {
      self.sheepCountUp();
    }, self.DURATION / 2);
  }
};

Sheep.prototype.onSheepAnimationEnd = function(e) {
  if (e.srcElement) {
    e.srcElement.parentElement.removeChild(e.srcElement);
  } else if (e.originalTarget) {
    e.originalTarget.parentElement.removeChild(e.originalTarget);
  }
};

Sheep.prototype.onSheepAnimationInteration = function(e) {
  var self = this;
  if (e.animationName.indexOf("sheep-run") === 0) {
    setTimeout(function() {
      self.sheepCountUp();
    }, self.DURATION / 2);
  }
};

Sheep.prototype.sheepCountUp = function() {
  var counter = document.getElementById("sheep_count");

  this.count++;
  counter.innerHTML = this.count + " sheep";
};

window.onload = function() {
  var sheep = new Sheep();
};

// vim: ts=2 sw=2

